package api;

import java.io.IOException;

import model.data_structures.IList;
import model.vo.VORoute;
import model.vo.VOStop;


/**
 * Basic API for testing the functionality of the STS manager
 */
public interface ISTSManager  {

	/**
	 * Method to load the routes of the STS
	 * @param routesFile - path to the file 
	 */
	public void loadRoutes(String routesFile) throws IOException;
	
	/**
	 * Method to load the trips of the STS
	 * @param tripsFile - path to the file 
	 * @throws Exception 
	 */
	public void loadTrips(String tripsFile) throws IOException;
	
	/**
	 * Method to load the times bus trips stop at a stop in the STS
	 * @param stopTimesFile - path to the file 
	 * @throws IOException 
	 */
	public void loadStopTimes(String stopTimesFile) throws IOException;
	
	/**
	 * Method to load the stops of the STS
	 * @param stopsFile - path to the file 
	 * @throws IOException 
	 */
	public void loadStops(String stopsFile) throws IOException;
	
	/**
	 * Method to calculate all the routes that stop at a given stop
	 * @param stopName - name of the stop to search routes for
	 * @return List of route objects making a stop at the given stop
	 */
	public IList<VORoute> routeAtStop(String stopName);
	
	/**
	 * Method to obtain all stops in a route. This stops must be ordered in accordance 
	 * to the route direction (distance from the closest to the furthers from the initial stop)
	 * @param routeName - name of the route to search for
	 * @param direction - directions in which we want to find the stops
	 * @return Ordered list of stops in the route for the given direction.
	 */
	public IList<VOStop> stopsRoute(String routeName, String direction);
}
