package controller;

import java.io.IOException;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {
	
	static String ruta="./data";
	

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes() throws IOException {
	manager.loadRoutes(ruta+"/routes.txt");
			
	}
		
	public static void loadTrips() throws IOException {
	manager.loadRoutes(ruta+"/routes.txt");
	manager.loadTrips(ruta+"/trips.txt");
	}

	public static void loadStopTimes()throws IOException {

	}
	
	public static void loadStops()throws IOException {
		manager.loadStops(ruta+"/stops.txt");
	}

	
	
	public static IList<VORoute> routeAtStop(String stopName) {
		return null;
	}
	
	public static IList<VOStop> stopsRoute(String routeName, String direction) {
		return null;
	}
}
