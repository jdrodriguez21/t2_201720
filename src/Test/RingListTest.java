package Test;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.RingList;

// en esta clase se prueban los mismos requerimientos que en la lista lineal 
// mas uno que garantizan que la lista funcione como una lista circular por lo que
// las pruebas consisten en las mismas para la doubleList mas unos reglones extra

public class RingListTest<T> extends TestCase {

	
	private RingList <T> List;
	private RingList <Integer>List1;
	private T a;
	public void setupEscenario1( )
    {
     List = new RingList <T> ();
         List.add(a);
    }
	
	public void setupEscenario2( )
    {
     List1 = new RingList<Integer> ();
     List1.add(1);
     List1.add(2);
     List1.add(3);
     List1.add(4);
     List1.add(5);
     List1.add(6);
     
    }
	
	
	 public void testList( )
	    {
	        setupEscenario1( );
            int s=List.getSize();
	        assertNotNull( "La lista List  no deberia ser nula", List );
	        assertEquals( "El tama�o de la lista deberia List ser 1", 1, s);
	          
	    }
	 public void testList1( )
	    {
	        setupEscenario2( );
           int s=List1.getSize();
	        assertNotNull( "La lista List1 No deberia ser nula ", List1 );
	        assertEquals( "El tama�o de la lista list1 deberia ser 6", 6, s);
	          
	        	        	    }
	 
	 public void testAdd()
	 { 
		 setupEscenario2( );
		 List1.add(9);
		 int p= List1.getElement(0);
		 assertEquals( "El numero accedido deberia ser 9", 9, p);   
		 
		 	 }
	 
	 public void testAddatEnd()
	 { 
		 setupEscenario2( );
		 List1.addAtEnd(9);
		 int p= List1.getElement(6);
		 assertEquals( "El numero accedido deberia ser 9", 9, p);  
		 
		 
		 
	 }
	 public void testGetElementK( )
	    {
	        setupEscenario2( );
        int s=List1.getElement(0);
        assertEquals( "El numero accedido deberia ser 6", 6, s);
        int k=List1.getElement(1);
        assertEquals( "El numero accedido deberia ser 5", 5, k);
        int m=List1.getElement(2);
        assertEquals( "El numero accedido deberia ser 4", 4, m);
        int l=List1.getElement(5);
        assertEquals( "El numero accedido deberia ser 1", 1, l);   
        
        // pruebas exclusivas para la lista circular
        
        
        int ss=List1.getElement(6);
        assertEquals( "El numero accedido deberia ser 6", 6, ss); 
        int sk=List1.getElement(7);
        assertEquals( "El numero accedido deberia ser 5", 5, sk); 
        
        int js=List1.getElement(8);
        assertEquals( "El numero accedido deberia ser 4", 4, js); 
        
	       }
	 public void testAddatK( )
	    {
	  setupEscenario2( );
	 
     List1.AddAtk(5,77);
     
     int k=List1.getElement(5);
     assertEquals( "El numero accedido deberia ser 77", 77, k);
     int p=List1.getElement(6);
     assertEquals( "El numero accedido deberia ser 1", 1, p);
     int pl=List1.getElement(4);
     assertEquals( "El numero accedido deberia ser 2", 2, pl);
     
     List1.AddAtk(0,97);
     int k1=List1.getElement(0);
     assertEquals( "El numero accedido deberia ser 97", 97, k1);
     // pruebas exclusivas para la lista circular
     
     List1.AddAtk(0,999);
     int kk2=List1.getElement(0);
     assertEquals( "El numero accedido deberia ser 999", 999, kk2);
	    
	    
	    
	    }
	  
	     
	 public void testDeleteatK( )
	    {
	  setupEscenario2( );
  List1.deleteATk(3);
  int k=List1.getElement(3);
  assertEquals( "El numero accedido deberia ser 2", 2, k);
	    }
	 
	 
	 public void testIterator( )
	    {
		 try{
	  setupEscenario2( );
	  Iterator<Integer>It=List1.iterator();
      int aa= It.next();
      assertEquals( "El numero accedido deberia ser 6", 6, aa);
      List1.AddAtk(2, 88);
      int bb= It.next();
      assertEquals( "El numero accedido deberia ser 5", 5, bb);
      
      int cc= It.next();
      assertEquals( "El numero accedido deberia ser 88",88, cc);
	    }
		 catch (Exception e)
		 {
			System.out.println(e); 
		 }
		 }
	 
	 
	 
	 
	
}
