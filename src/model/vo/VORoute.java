package model.vo;

/**
 * Representation of a route object
 */


public class VORoute {

private int id;
private String Name;

	

public VORoute(int ID, String name ){
	
id=ID;
Name=name;
	
}
/**
 * @return id - Route's id number
 */
	public int id() {
	
		return id;
		
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		return Name;
	
	}

}
