package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {
	
	private int id;
	private String Name;
	private int zoneId;
	
	public VOStop(int Id, String name,int zoneid){
	
		id=Id;
		Name=name;
		zoneId=zoneid;
			
	}
	
	

	/**
	 * @return id - stop's id
	 */
	public int id() {
	
		return id;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
	
		return Name;
	}
	public int ZoneId() {
		
		return zoneId;
	}


}
