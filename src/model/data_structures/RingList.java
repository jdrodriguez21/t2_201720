package model.data_structures;

import java.util.Iterator;

import javax.print.attribute.standard.PrinterLocation;

public class RingList <T> implements IList<T>   {

	private Node First;
	private int size=0;
	private Node Last;

	public class Node 
	{
		T item;
		public Node(T t)
		{
			item =t;
		}

		Node next;
		Node previous;
		public T getItem()
		{
			return item;
		}

		public Node getNext()
		{

			return next;
		}

		public Node getPrevious()
		{
			return previous;
		}
		public void setNext(Node n)
		{
			next=n;
		}
		public void setPrevious(Node p)
		{
			previous=p;
		}

	}

	public Iterator<T> iterator()
	{ return new ListIterator(); }

	private class ListIterator implements Iterator<T>
	{
		private Node actual = First;
		public boolean hasNext()
		{ return actual != null; }
		public void remove() { }
		public T next()
		{
			T item = actual.item;
			actual = actual.next;
			return item;
		}
	}


	public Integer getSize() {
		return size;	}

	@Override
	public void add(T t) {
		Node a = new Node( t);
		if (size==0)
		{			
			First=a;
			Last=First;
			First.setNext(First);
			First.setPrevious(First);
		}
		else{

			First.setPrevious(a);
			a.setNext(First);
			First=a;
			First.setPrevious(First);
			Last.setNext(First);

		}
		size ++;	
	}

	@Override
	public void addAtEnd(T t) {
		Node n=  new Node(t);
		if (size==0)
		{			
			First=n;
			Last=First;
			First.setPrevious(Last);
			Last.setNext(First);

		}
		else{

			Last.setNext(n);
			n.setPrevious(Last);
			Last=n;
			Last.setNext(First);
			First.setPrevious(Last);
		}
		size ++;
	}

	@Override
	public void AddAtk(int k, T t)  {

		k=k%size;
		
		{

			Node a = new Node(t);
			if (size==0)
			{
				First=a;
				Last=First;
				First.setNext(Last);
				Last.setNext(First);
			}
			else{
			
					if (k!=0){

						Node c= getNode(k);
						Node d=c.getPrevious();
						a.setPrevious(d);
						a.setNext(c);
						d.setNext(a);
						c.setPrevious(a);
					}
					else 
					{ 		 Node c= getNode(k);

					a.setNext(c);

					c.setPrevious(a);
					First=a;
					First.setNext(Last);
					Last.setNext(First);
					}
				
			}

			size ++;	
		}}




	private Node getNode(int k) {
	
		k=k%size;
			Node a =First;

			for (int i=0 ; i<k;i++)
			{
				a=a.getNext();

			}


			return a;
	}




	@Override
	public void deleteATk(int k) {
		try{
			Node  a=getNode(k);
			Node b=a.getPrevious();
			Node c= a.getNext();

			b.setNext(c);
			c.setPrevious(b);
			size--;}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}


	@Override
	public T getElement(int k) {
		
			Node a= getNode(k);
			T b= a.getItem();
			return b;
		}
	



}
