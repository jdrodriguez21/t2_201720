package model.logic;



import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOtrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

public class STSManager implements ISTSManager  {

	private File archivo;
	private DoubleLinkedList<VORoute> Routes;
	private DoubleLinkedList<VOStopTime> StopTime;
	private DoubleLinkedList<String> zonas;
	private RingList<DoubleLinkedList<VOStop>> Stops;
	private GregorianCalendar cal;
	
	private DoubleLinkedList<DoubleLinkedList<VOtrip>> Trips;


	
	public static int getDayOfTheWeek(Date d){
		int a=5;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);		

		
	}
	
	
	
	
	@Override
	public void loadRoutes(String routesFile) throws IOException {
		Routes = new  DoubleLinkedList<VORoute>();
		
		
		archivo = new File (routesFile);
		if (archivo==null)
		{
			throw new IOException("El archivo que se intenta abrir es nulo");
		}
		else
		{
		FileReader reader = new FileReader (archivo);
		
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		while (Linea!=null)
		{

			String[] a= Linea.split(",");
			String name=a[3];
			int Id=Integer.parseInt(a[0]);
			VORoute Route= new VORoute(Id, name);
			Routes.addAtEnd(Route);
		
			Linea=lector.readLine();
		}
	

		lector.close();
		reader.close();
	}}

	@Override
	public void loadTrips(String tripsFile) throws IOException
		{
			Trips= new DoubleLinkedList<DoubleLinkedList<VOtrip>>(); 

			archivo = new File (tripsFile);
			if (archivo==null)
			{
				throw new IOException("El archivo que se intenta abrir es nulo");
			}
			else
			{
				
			FileReader reader = new FileReader (archivo);
			
			BufferedReader lector = new BufferedReader(reader);
			String Linea=lector.readLine();

			
			while (Linea!=null)
			{	
				String[] a= Linea.split(",");
				Linea=lector.readLine();
														
			}
			
			lector.close();
			reader.close();
		}}

		
			
		
		


	@Override
	public void loadStopTimes(String stopTimesFile) throws IOException {
  Routes = new  DoubleLinkedList<VORoute>();
		
		archivo = new File (stopTimesFile);

		FileReader reader = new FileReader (archivo);

		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		while (Linea!=null)
		{ 
			String[] a= Linea.split(",");
			
			int TripId=Integer.parseInt(a[0]);
			String ArrivalT=a[1];
			String DepartureT=a[2];
			int StopId=Integer.parseInt(a[3]);
			VOStopTime k= new VOStopTime(StopId, ArrivalT, DepartureT, TripId);
			StopTime.add(k);
			Linea=lector.readLine();
		}
	
		lector.close();
		reader.close();

	}

	@Override
	public void loadStops(String stopsFile)  throws IOException {
		zonas= new  DoubleLinkedList<String>();
		Stops = new RingList<DoubleLinkedList<VOStop>>();
		archivo = new File (stopsFile);
		FileReader reader = new FileReader (archivo);
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		
		// a continuacion se crea un arreglo de las zonas de la ciudad contenidas en las
		// stops
		
		while (Linea!=null)
		{
           String[] a= Linea.split(",");
		 String Zone=a[6]; 
		 System.out.println(Zone);
		 boolean Agregado=false;
		  
		 Iterator<String>It=zonas.iterator();

			while(It.hasNext()&&Agregado==false)
			{ 				
			String aa=It.next();
		     if(aa.compareTo(Zone)==0)
		     {
		    	 Agregado=true;
		     }
		   
			}
			if(Agregado==false)
			{
				zonas.add(Zone);
			}
		 Linea=lector.readLine();
		}
		lector.close();
	
		// A continuación se crea una lista de paradas por cada zona de la lista anterior
		for (int i=0;i<zonas.getSize();i++)
		{
			DoubleLinkedList<VOStop> Stt= new DoubleLinkedList<VOStop>();
			Stops.add(Stt);
		}
		// A continuacion agregamos las paradas que le corresponde a cada zona
	
		
	
	}
	

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		return null;

	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
